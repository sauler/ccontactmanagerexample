/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CContactManagerExample for AQQ                         *
 *                                                                             *
 * CContactManagerExample plugin is free software: you can redistribute it     *
 * and/or  modify it under the terms of the GNU General Public License as      *
 * published by the Free Software Foundation; either version 3,                *
 * or (at your option) any later version.                                      *
 *                                                                             *
 * CContactManagerExample is distributed in the hope that it will be useful,   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include <vcl.h>
#include <windows.h>

// SDK includes
#include "SDK\PluginLink.h"
#include "SDK\PluginAPI.h"
#include "SDK\Contact.h"
#include "SDK\ContactManager.h"

TPluginInfo PluginInfo;
CContactManager* ContactManager;

int WINAPI DllEntryPoint(HINSTANCE hinst,unsigned long reason,void* lpReserved)
{
	return 1;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Load(PPluginLink Link)
{
	CPluginLink::instance()->SetLink(*Link);

	ContactManager = new CContactManager();
	ContactManager->AddContact(new CContact("example@plugin"));
	ContactManager->GetLastAddedContact()->Nick = "Examplec";
	ContactManager->GetLastAddedContact()->Groups = "Example1";
	ContactManager->GetLastAddedContact()->UserIDx = 0;
	ContactManager->GetLastAddedContact()->FromPlugin = true;
	ContactManager->GetLastAddedContact()->Subscription = SUB_BOTH;
	ContactManager->GetLastAddedContact()->Create();

	ContactManager->AddContact(new CContact("example1@plugin"));
	ContactManager->GetLastAddedContact()->Nick = "Examplec1";
	ContactManager->GetLastAddedContact()->Groups = "Example1";
	ContactManager->GetLastAddedContact()->UserIDx = 0;
	ContactManager->GetLastAddedContact()->FromPlugin = true;
	ContactManager->GetLastAddedContact()->Subscription = SUB_BOTH;
	ContactManager->GetLastAddedContact()->Create();

	return 0;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Unload()
{
    delete ContactManager;
	return 0;
}

extern "C" __declspec(dllexport) PPluginInfo __stdcall AQQPluginInfo
	(DWORD AQQVersion)
{
	PluginInfo.cbSize = sizeof(TPluginInfo);
	PluginInfo.ShortName = L"CContactManagerExample";
	PluginInfo.Version = PLUGIN_MAKE_VERSION(1,0,0,0);
	PluginInfo.Description =
		L"Przyk�adowa wtyczka u�ywaj�ca mened�era kontakt�w dostarczonego przez SDK.";
	PluginInfo.Author = L"Rafa� Babiarz (sauler)";
	PluginInfo.AuthorMail = L"sauler1995@gmail.com";
	PluginInfo.Copyright = L"sauler";
	PluginInfo.Homepage = L"";
	PluginInfo.Flag = 0;
	PluginInfo.ReplaceDefaultModule = 0;

	return &PluginInfo;
}

